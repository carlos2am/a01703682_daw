<?php

function conectDb() {
    //$servername = "localhost";
    //$username = "root";
    //$password = "";
    //$dbname = "jurassic";                           //DATOS PARA EL SERVIDOR
    $servername = "mysql1008.mochahost.com";
    $username = "dawbdorg_1703682";
    $password = "1703682";
    $dbname = "dawbdorg_A01703682";

    $con = mysqli_connect($servername, $username, $password, $dbname);

    //Check connection
    if (!$con) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $con;
}

function closeDb($mysql) {
     mysqli_close($mysql);
}

function getIncidentes() {
    $conn = conectDb();
    $sql = "CALL getIncidentes();";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function getIncidentesMasRecientes() {
    $conn = conectDb();
    $sql = "CALL getIncidentesMasRecientes();";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function nuevoIncidente($lugar,$tipo) {
    $conn = conectDb();
    $sql = "CALL nuevoIncidente($lugar,$tipo);";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function getIncidentesPorLugar($lugar){
    $conn = conectDb();
    $sql = "CALL getIncidentesPorLugar('$lugar');";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function showQuery($result){
    if(mysqli_num_rows($result) > 0){
        echo '<table><tr>';
        echo '<th>'.'Lugar'.'</th>';
        echo '<th>'.'Tipo de Incidente'.'</th>';
        echo '<th>'.'Fecha y Hora'.'</th>';
        echo '</tr>';
        while($row = mysqli_fetch_assoc($result)){
            echo '<tr>';
            echo '<td>'.$row['Lugar'].'</td>';
            echo '<td>'.$row['Incidente'].'</td>';
            echo '<td>'.$row['hora'].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}

function select1(){
    $conn = conectDb();
    $sql = "CALL getLugares();";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    if(mysqli_num_rows($result) > 0){
        echo '<div class="input-field"><select class="browser-default" name="lugar"><option value="" disabled selected>Selecciona el lugar</option>';
        while($row = mysqli_fetch_assoc($result)){
            echo '<option value='.$row['idLugar'].'>'.$row['nombre'].'</option>';
        }
        echo '</select></div>';
    }
}

function select2(){
    $conn = conectDb();
    $sql = "CALL getTipos();";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    if(mysqli_num_rows($result) > 0){
        echo '<div class="input-field"><select onchange="showCD(this.value)" class="browser-default" name="tipo"><option value="" disabled selected>Selecciona el incidente</option>';
        while($row = mysqli_fetch_assoc($result)){
            echo '<option value='.$row['idTipo'].'>'.$row['nombre'].'</option>';
        }
        echo '</select></div>';
    }
}

?>