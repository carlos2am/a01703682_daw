DROP TABLE IF EXISTS incidentes;
DROP TABLE IF EXISTS lugares;
DROP TABLE IF EXISTS tipos;

-- TABLAS
CREATE TABLE `lugares` (
  `idLugar` int(40) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nombre` varchar(40)
);

CREATE TABLE `tipos` (
  `idTipo` int(40) NOT NULL PRIMARY KEY,
  `nombre` varchar(40)
);

CREATE TABLE `incidentes` (
  `idIncidente` int(40) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `idLugar` int(40) NOT NULL,
  `idTipo` int(40) NOT NULL,
  `hora` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   FOREIGN KEY (`idLugar`) REFERENCES `lugares`(`idLugar`),
   FOREIGN KEY (`idTipo`) REFERENCES `tipos`(`idTipo`)
);

INSERT INTO `tipos` (`idTipo`,`nombre`) VALUES
(1,'Falla electrica'),
(2,'Fuga de herbivoro'),
(3,'Fuga de Velociraptors'),
(4,'Fuga de TRex'),
(5,'Robo de ADN'),
(6,'Auto descompuesto'),
(7,'Visitantes en zona no autorizada');

INSERT INTO `lugares` (`nombre`) VALUES
('Centro turistico'),
('Laboratorios'),
('Restaurante'),
('Centro operativo'),
('Triceratops'),
('Dilofosaurios'),
('Velociraptors'),
('TRex'),
('Planicie de los herbivoros');

INSERT INTO `incidentes` (idLugar,idTipo) VALUES
(2,2),
(1,1),
(3,3);

-- STORED PROCEDURES

/*USE `jurassic`; nombre de la base de datos - CAMBIAR*/
USE `dawbdorg_A01703682`;
DROP procedure IF EXISTS `getIncidentes`;

DELIMITER $$

CREATE PROCEDURE `getIncidentes` ()
BEGIN
SELECT l.nombre as Lugar,t.nombre as Incidente,hora
    FROM incidentes as i, tipos as t, lugares as l
    WHERE i.idLugar=l.idLugar AND i.idTipo=t.idTipo;
END$$

DELIMITER ;

DROP procedure IF EXISTS `getIncidentesMasRecientes`;

DELIMITER $$

CREATE PROCEDURE `getIncidentesMasRecientes` ()
BEGIN
SELECT l.nombre as Lugar,t.nombre as Incidente,hora
    FROM incidentes as i, tipos as t, lugares as l
    WHERE i.idLugar=l.idLugar AND i.idTipo=t.idTipo
    ORDER BY hora DESC;
END$$

DELIMITER ;

DROP procedure IF EXISTS `nuevoIncidente`;

DELIMITER $$

CREATE PROCEDURE `nuevoIncidente` (IN lugarN int,IN tipoN int)
BEGIN
INSERT INTO `incidentes` (`idLugar`,`idTipo`) VALUES
    (lugarN,tipoN);
END$$

DELIMITER ;

DROP procedure IF EXISTS `getIncidentesPorLugar`;

DELIMITER $$

CREATE PROCEDURE `getIncidentesPorLugar` (IN lugarN VARCHAR(50))
BEGIN
SELECT l.nombre as Lugar,t.nombre as Incidente,hora
    FROM incidentes as i, tipos as t, lugares as l
    WHERE i.idLugar=l.idLugar AND i.idTipo=t.idTipo AND l.nombre LIKE CONCAT('%', lugarN , '%');
END$$

DELIMITER ;

DROP procedure IF EXISTS `getLugares`;

DELIMITER $$

CREATE PROCEDURE `getLugares` ()
BEGIN
SELECT *
    FROM lugares;
END$$

DELIMITER ;

DROP procedure IF EXISTS `getTipos`;

DELIMITER $$

CREATE PROCEDURE `getTipos` ()
BEGIN
SELECT *
    FROM tipos;
END$$

DELIMITER ;



