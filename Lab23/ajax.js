if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
function consulta(x) {
    if (x=="") {
      document.getElementById("query").innerHTML="";
      return;
    }
    xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
        document.getElementById("query").innerHTML=this.responseText;
      }
    }
    xmlhttp.open("GET","controladorConsulta.php?q="+x,true);
    xmlhttp.send();
  }

  function agregar() {
    xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
        document.getElementById("contenido").innerHTML=this.responseText;
      }
    }
    xmlhttp.open("GET","controladorAgregar.php",true);
    xmlhttp.send();
  }

  function eliminar() {
    xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
        document.getElementById("contenido").innerHTML=this.responseText;
      }
    }
    xmlhttp.open("GET","controladorEliminar.php",true);
    xmlhttp.send();
  }

  function modificar() {
    xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
        document.getElementById("contenido").innerHTML=this.responseText;
      }
    }
    xmlhttp.open("GET","controladorModificar.php",true);
    xmlhttp.send();
  }


  $(document).ready(function(){
    $("button").click(function(){
      $("#query").html('<img id="theImg" src="diagrama.png" />')
    });
  });

  function recaptcha_callback(){
    document.getElementById('iniciosesion').disabled = false;
  }
  
