DROP TABLE IF EXISTS Entregan;
DROP TABLE IF EXISTS Materiales;
DROP TABLE IF EXISTS Proyectos;
DROP TABLE IF EXISTS Proveedores;


CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) ;

CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) ;



CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) ;



CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha Date not null, 
  Cantidad numeric (8,2) 
) ;


ALTER TABLE `Materiales` 
ADD PRIMARY KEY (`Clave`);
ALTER TABLE `Proyectos` 
ADD PRIMARY KEY (`Numero`);
ALTER TABLE `Proveedores` 
ADD PRIMARY KEY (`RFC`);

ALTER TABLE `Entregan` 
ADD PRIMARY KEY (`Clave`,`RFC`,`Numero`,`Fecha`),
ADD CONSTRAINT `EntreganTieneRFC` FOREIGN KEY (`RFC`) REFERENCES `Proveedores` (`RFC`),
ADD CONSTRAINT `EntreganTieneProyectos` FOREIGN KEY (`Numero`) REFERENCES `Proyectos` (`Numero`),
ADD CONSTRAINT `EntreganTieneMateriales` FOREIGN KEY (`Clave`) REFERENCES `Materiales` (`Clave`);





LOAD DATA INFILE 'materiales.csv' 
INTO TABLE lab16.materiales
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


LOAD DATA INFILE 'proveedores.csv' 
INTO TABLE lab16.proveedores
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


LOAD DATA INFILE 'proyectos.csv' 
INTO TABLE lab16.proyectos
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


LOAD DATA INFILE 'entregan.csv' 
INTO TABLE lab16.entregan
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(Clave,RFC,Numero,@Fecha,Cantidad)
SET Fecha = STR_TO_DATE(@Fecha, '%d/%m/%Y');