function validar(){
    cont= document.getElementById("x").value;
    cont2= document.getElementById("y").value;
    if(cont != cont2){
        document.getElementById("v").innerHTML="No coinciden"
    }
    else{
        document.getElementById("v").innerHTML="Las contraseñas coinciden"
    }

}

function comprar(){
    let hot,ham,tac;
    let iva=0;
    let sum;
    hot= document.getElementById("hot").value;
    ham= document.getElementById("ham").value;
    tac= document.getElementById("tac").value;
    if(hot!=0){
        iva+=(hot*0.16)
        //hot=hot*1.16;
    }
    if(ham!=0){
        iva+=(ham*0.16)
        //ham=ham*1.16;
    }
    if(tac!=0){
        iva+=(tac*0.16)
        //tac=tac*1.16;
    }
    sum=parseFloat(hot)+parseFloat(ham)+parseFloat(tac)+parseFloat(iva);
    document.getElementById("c").innerHTML="HotDog: $"+hot+"<br>Hamburguesa: $"+ham+"<br>Tacos: $"+tac+"<br>IVA: $"+iva+"<br>Total: $"+sum

}

function calculadora(){
    let x,y,op,res=0;
    x= document.getElementById("num1").value;
    y= document.getElementById("num2").value;
    op= document.getElementById("op").value;

    if(op==1){
        res=parseFloat(x)+parseFloat(y);
    }
    else if(op==2){
        res=parseFloat(x)-parseFloat(y);
    }
    else if(op==3){
        res=parseFloat(x)*parseFloat(y);
    }
    else{
        if(y==0){
            res="Ind"
        }
        else{
        res=parseFloat(x)/parseFloat(y);
        }
    }

    document.getElementById("calc").innerHTML="Resultado: "+res
}

function allowDrop(ev) {
    ev.preventDefault();
  }
  
  function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }
  
  function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
  }

  function colores(){
    let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
    document.getElementById("title").style.color= randomColor;
    let randomColor2 = '#'+Math.floor(Math.random()*16777215).toString(16);
    document.getElementById("name").style.color= randomColor2;

  }

  function arial(){
    document.getElementById("title").style.fontFamily = "Arial";
    document.getElementById("name").style.fontFamily = "Arial";
  }

  function italic(){
    document.getElementById("title").style.fontStyle = "italic";
    document.getElementById("name").style.fontStyle = "italic";
  }
  

  function encima(){
    document.getElementById("ayuda").innerHTML = "Esta es una función donde se muestra un texto al pasar por encima del texto";
    
  }

  function fuera(){
    document.getElementById("ayuda").innerHTML = "Coloca el mouse encima del texto";
  }