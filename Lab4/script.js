function problema1(){
    let n=prompt("Da un número")
    document.write("<table>")
    for(i=1;i<=n;i++){
        document.write("Número: "+i+" Cuadrado: "+Math.pow(i,2)+" Cúbico: "+Math.pow(i,3)+"<br>");

    }
    document.write("</table>")
}

function problema2(){
    let n1= Math.floor(Math.random()*30);
    let n2= Math.floor(Math.random()*30);
    let r=n1+n2;
    let n=prompt("¿Cual es el resultado de "+n1+" + "+n2)
    if(n==r){
        document.write("Correcto")
    }
    else{
        document.write("Incorrecto")
    }
}

function problema3(){
    let arreglo= [-5,-2,0,0,5,6]
    let neg=0,pos=0,ceros=0;
    for(i=0;i<arreglo.length;i++){
        if(arreglo[i]<0){
            neg++;
        }
        else if(arreglo[i]>0){
            pos++;
        }
        else{
            ceros++;
        }
    }
    for(j=0;i<arreglo.length;j++){
        document.write(""+arreglo[j])
    }
    document.write(+arreglo[0]+" "+arreglo[1]+" "+arreglo[2]+" "+arreglo[3]+" "+arreglo[4]+" "+arreglo[5]+"<br>Menores a 0: "+neg+"<br>Ceros: "+ceros+"<br>Mayores a 0: "+pos);
}

function problema4(){
    let arreglo= [[1,2,3],[4,5,6],[7,8,9]];
    let x=0;
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            x+=arreglo[i][j];
        }
        x=x/3;
        document.write("Promedio: "+x)
        x=0;
    }
}

function problema5(){
    let numero= 1234567;
    document.write("Número: "+numero+"<br>Inverso: ")
    while(Math.round(numero/10) > 0 && numero >= 10){
        document.write(parseInt(numero%10));
        parseInt(numero = numero/ 10, 10);
    }
    document.write(+parseInt(numero,10))
}

//Problema 6

class Radio{
    constructor(volume, power){
        this.volume=50;
        this.power=false;
    }

    volumeup(){
        if(this.volume<100){
            this.volume+=5;
        }
    }

    volumedown(){
        if(this.volume>0){
            this.volume-=5;
        }
    }

    powerOnOff(){
        if(this.power==true){
            this.power=false;
        }
        else{
            this.power=true;
        }
    }
}

var radio = new Radio;
function estado(){
    
    alert("Volume: "+radio.volume+"  Power: "+radio.power);
}

function volumeup(){
    radio.volumeup();
    alert("Volume: "+radio.volume+"  Power: "+radio.power);
}

function volumedown(){
    radio.volumedown();
    alert("Volume: "+radio.volume+"  Power: "+radio.power);
}

function power(){
    radio.powerOnOff();
    alert("Volume: "+radio.volume+"  Power: "+radio.power);
}