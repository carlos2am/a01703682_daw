<?php
    require_once("util.php");
    include("partials/_header.html");
    echo "<div id=\"contenido\">";
    include("partials/_consultaFrutas.html");
    echo "<div id=\"query\" class=\"container\">";
  
    if(isset($_POST['opcion'])){
        $nombre = $_POST["opcion"];
        showQuery(getFrutasPorNombre($nombre));
    }

    if(empty($_POST['opcion'])){
        showQuery(getFrutas());
    }

    echo "</div>";
    echo "</div>";
    include("partials/_footer.html"); 

 
    
?>