<?php

function conectDb() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "frutas";

    $con = mysqli_connect($servername, $username, $password, $dbname);

    //Check connection
    if (!$con) {
        die("Connection failed: " . mysqli_connect_error());
    }

    return $con;
}

function closeDb($mysql) {
     mysqli_close($mysql);
}

function getFrutas() {
    $conn = conectDb();
    $sql = "call getFrutas()";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function getFrutasPorNombre($nombreFruta){
    $conn = conectDb();
    $sql = "call getFrutasPorNombre('$nombreFruta')";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function nuevaFruta($nombre,$unidades,$cantidad,$precio,$pais) {
    $conn = conectDb();
    $sql = "call nuevaFruta('$nombre',$unidades,$cantidad,$precio,'$pais')";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function eliminarFruta($id) {
    $conn = conectDb();
    $sql = "call eliminarFruta($id)";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function modificarFruta($id,$nombre,$unidades,$cantidad,$precio,$pais) {
    $conn = conectDb();
    $sql = "call modificarFruta($id,'$nombre',$unidades,$cantidad,$precio,'$pais')";
    $result = mysqli_query($conn, $sql);
    closeDb($conn);

    return $result;
}

function showQuery($result){
    if(mysqli_num_rows($result) > 0){
        echo '<table><tr>';
        echo '<td>'.'ID'.'</td>';
        echo '<td>'.'Nombre'.'</td>';
        echo '<td>'.'Unidades'.'</td>';
        echo '<td>'.'Cantidad'.'</td>';
        echo '<td>'.'Precio'.'</td>';
        echo '<td>'.'País'.'</td>';
        echo '</tr>';
        while($row = mysqli_fetch_assoc($result)){
            echo '<tr>';
            echo '<td>'.$row['id'].'</td>';
            echo '<td>'.$row['nombre'].'</td>';
            echo '<td>'.$row['unidades'].'</td>';
            echo '<td>'.$row['cantidad'].'</td>';
            echo '<td>'.$row['precio'].'</td>';
            echo '<td>'.$row['pais'].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}

?>