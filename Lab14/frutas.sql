DROP TABLE IF EXISTS frutas;

CREATE TABLE `frutas` (
  `nombre` varchar(40),
  `unidades` int(11),
  `cantidad` int(11),
  `precio` int(11),
  `pais` varchar(40)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `frutas` (`nombre`,`unidades`,`cantidad`,`precio`,`pais`) VALUES
('manzana',10,5,5,'Mexico'),
('platano',20,5,7,'Mexico'),
('pera',23,10,5,'China'),
('mango',30,1,10,'Brazil'),
('manzana',10,5,5,'China');