<?php
    require_once("util.php");
    include("partials/_header.html");
    //showQuery(getFrutas());
    include("partials/_consultaFrutas.html");
    
    echo "<div class=\"container\">";
    if(isset($_POST['opcion'])){
        $nombre = $_POST["opcion"];
        showQuery(getFrutasPorNombre($nombre));
    }

    if(empty($_POST['opcion'])){
        showQuery(getFrutas());
    }
    echo "</div>";
   
    include("partials/_footer.html"); 

 
    
?>